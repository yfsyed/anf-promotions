package com.anf.test;

import com.anf.test.SampleData;
import com.anf.test.model.Promotions;

import org.json.JSONException;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class PromotionsTest {

    Promotions mPromotions;

    @Test
    public void emptyPromotionsTest() {
        mPromotions = new Promotions();
        assertNotNull("List should not be null", mPromotions.getPromotions());
        assertNull("ErrorMessage should be null", mPromotions.getErrorMessage());
    }

    @Test
    public void nullPromotionsTest() {
        boolean flag = false;
        try {
            mPromotions = new Promotions(null);
        } catch (NullPointerException je) {
            flag = true;
        } catch (JSONException je) {
            fail("Incorrect Exception");
        }
        assertTrue("Must throw Exception", flag);
        assertNull("Promotions should be null", mPromotions);
    }

    @Test
    public void randomPromotionsTest() {
        boolean flag = false;
        try {
            mPromotions = new Promotions("{sdflkafl_sdjflas{]}");
        } catch (JSONException je) {
            flag = true;
        }
        assertTrue("Must throw Exception", flag);
        assertNull("Promotions should be null", mPromotions);
    }

    @Test
    public void promotionsSuccessTest() {
        try {
            mPromotions = new Promotions(SampleData.TEST_DATA);
            assertNotNull("List should not be null", mPromotions.getPromotions());
            assertNotNull("List should not be null", mPromotions.getPromotions());
            assertTrue("List should not be null", mPromotions.getPromotions().size() == 2);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }
    }

}
