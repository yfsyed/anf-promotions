package com.anf.test;

import com.anf.test.SampleData;
import com.anf.test.model.ANFButton;
import com.anf.test.model.Promotion;
import com.anf.test.model.Promotions;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class PromotionTest {

    Promotion mPromotion;

    @Test
    public void emptyPromotionTest1() {
        mPromotion = new Promotion();
        asserts();
    }

    @Test
    public void emptyPromotionTest2() {
        try {
            JSONObject jsonObject = new JSONObject("{}");
            mPromotion = new Promotion(jsonObject);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }
        assertNotNull("Promotion should not be null", mPromotion);
        assertEquals("Titles should be same", "",  mPromotion.getTitle());
        assertEquals("Footers should be same", "", mPromotion.getFooter());
        assertEquals("Description should be same", "",  mPromotion.getDescription());
        assertEquals("image should be same", "",  mPromotion.getImage());
        assertTrue("Buttons should not be null", mPromotion.getAnfButtonList().size() == 0);
    }

    @Test
    public void nullPromotionTest() {
        mPromotion = new Promotion(null);
        asserts();
    }

    @Test
    public void randomPromotionTest() {
        boolean flag = false;
        try {
            JSONObject jsonObject = new JSONObject("{sdflkafl_sdjflas{]}");
            mPromotion = new Promotion(jsonObject);
        } catch (JSONException je) {
            flag = true;
        }
        assertTrue("Must throw Exception", flag);
        assertNull("Promotion should be null", mPromotion);
    }

    @Test
    public void promotionSuccessTest1() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_1);
            mPromotion = new Promotion(jsonObject);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }
        assertNotNull("Promotion should not be null", mPromotion);
        assertEquals("Titles should be same", "Shorts Starting at $25",  mPromotion.getTitle());
        assertEquals("Footers should be same", "In stores & online. Exclusions apply. <a href=\"https://www.abercrombie.com/anf/media/legalText/viewDetailsText20150618_Shorts25_US.html\" class=\"legal promo-details\">See details</a>", mPromotion.getFooter());
        assertEquals("Description should be same", "GET READY FOR SUMMER DAYS",  mPromotion.getDescription());
        assertEquals("image should be same", "http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-shorts",  mPromotion.getImage());
        assertTrue("Button should not be null", mPromotion.getAnfButtonList().size() == 1);
    }

    @Test
    public void promotionSuccessTest2() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_2);
            mPromotion = new Promotion(jsonObject);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }
        assertNotNull("Promotion should not be null", mPromotion);
        assertEquals("Titles should be same", "Dolce Vita",  mPromotion.getTitle());
        assertEquals("Footers should be same", "", mPromotion.getFooter());
        assertEquals("Description should be same", "Our Favorite Brands",  mPromotion.getDescription());
        assertEquals("image should be same", "http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-brands",  mPromotion.getImage());
        assertTrue("Buttons should not be null", mPromotion.getAnfButtonList().size() == 1);
    }

    public void asserts(){
        assertNotNull("Promotion should not be null", mPromotion);
        assertNotNull("ButtonList should not be null", mPromotion.getAnfButtonList());
        assertNull("Title should be null", mPromotion.getTitle());
        assertNull("description should be null", mPromotion.getDescription());
        assertNull("footer should be null", mPromotion.getFooter());
        assertNull("image should be null", mPromotion.getImage());
    }
}
