package com.anf.test;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class SampleData {
    public static final String TEST_DATA = "{\n" +
            "  \"promotions\": [\n" +
            "    {\n" +
            "      \"button\": {\n" +
            "        \"target\": \"https://m.abercrombie.com\", \n" +
            "        \"title\": \"Shop Now\"\n" +
            "      }, \n" +
            "      \"description\": \"GET READY FOR SUMMER DAYS\", \n" +
            "      \"footer\": \"In stores & online. Exclusions apply. <a href=\\\"https://www.abercrombie.com/anf/media/legalText/viewDetailsText20150618_Shorts25_US.html\\\" class=\\\"legal promo-details\\\">See details</a>\", \n" +
            "      \"image\": \"http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-shorts\", \n" +
            "      \"title\": \"Shorts Starting at $25\"\n" +
            "    }, \n" +
            "    {\n" +
            "      \"button\": [\n" +
            "        {\n" +
            "          \"target\": \"https://m.hollisterco.com\", \n" +
            "          \"title\": \"Shop Now\"\n" +
            "        }\n" +
            "      ], \n" +
            "      \"description\": \"Our Favorite Brands\", \n" +
            "      \"image\": \"http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-brands\", \n" +
            "      \"title\": \"Dolce Vita\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static final String TEST_DATA_1 = "{\n" +
            "  \"promotions\": [\n" +
            "    {\n" +
            "      \"button\": {\n" +
            "        \"target\": \"https://m.abercrombie.com\", \n" +
            "        \"title\": \"Shop Now\"\n" +
            "      }, \n" +
            "      \"description\": \"GET READY FOR SUMMER DAYS\", \n" +
            "      \"footer\": \"In stores & online. Exclusions apply. <a href=\\\"https://www.abercrombie.com/anf/media/legalText/viewDetailsText20150618_Shorts25_US.html\\\" class=\\\"legal promo-details\\\">See details</a>\", \n" +
            "      \"image\": \"http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-shorts\", \n" +
            "      \"title\": \"Shorts Starting at $25\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";


    public static final String TEST_ITEM_1 = "{\n" +
            "\"button\": {\n" +
            "\"target\": \"https://m.abercrombie.com\",\n" +
            "\"title\": \"Shop Now\"\n" +
            "},\n" +
            "\"description\": \"GET READY FOR SUMMER DAYS\",\n" +
            "\"footer\": \"In stores & online. Exclusions apply. <a href=\\\"https://www.abercrombie.com/anf/media/legalText/viewDetailsText20150618_Shorts25_US.html\\\" class=\\\"legal promo-details\\\">See details</a>\",\n" +
            "\"image\": \"http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-shorts\",\n" +
            "\"title\": \"Shorts Starting at $25\"\n" +
            "}";

    public static final String TEST_ITEM_2 = "{\n" +
            "\"button\": [\n" +
            "{\n" +
            "\"target\": \"https://m.hollisterco.com\",\n" +
            "\"title\": \"Shop Now\"\n" +
            "}\n" +
            "],\n" +
            "\"description\": \"Our Favorite Brands\",\n" +
            "\"image\": \"http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-brands\",\n" +
            "\"title\": \"Dolce Vita\"\n" +
            "}";

    public static final String TEST_Button_1 = "{\n" +
            "\"target\": \"https://m.abercrombie.com\",\n" +
            "\"title\": \"Shop Now\"\n" +
            "}";


    public static final String TEST_Button_2 = "{\n" +
            "\"target\": \"https://m.hollisterco.com\",\n" +
            "\"title\": \"Shop Now\"\n" +
            "}";
}
