package com.anf.test;

import com.anf.test.listeners.IPresenterCallback;
import com.anf.test.model.Promotion;
import com.anf.test.model.Promotions;
import com.anf.test.presenter.ANFListPresenter;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Yousuf Syed on 6/30/16.
 */
public class ANFListPresenterTest {

    private Promotion promotion;
    private List<Promotion> promotionList;
    private final String errorMsg = "Network unavailable";
    private boolean showProgress = true;
    private ANFListPresenter mPresenter;

    @After
    public void tearDown(){
        promotion = null;
        promotionList = null;
        mPresenter = null;
    }

    @Test
    public void successfulUpdateProgressTest() {
        mPresenter = new ANFListPresenter();
        mPresenter.setCallbackListener(positivePresenter);
        mPresenter.updateProgress();
    }

    @Test
    public void negativeUpdateProgressTest() {
        mPresenter = new ANFListPresenter();
        mPresenter.setCallbackListener(negativePresenter);
        showProgress = false;
        mPresenter.updateProgress();
    }

    @Test
    public void successfulSendResultTest() {
        try {
            Promotions promotions = new Promotions(SampleData.TEST_DATA);
            promotionList = promotions.getPromotions();

            mPresenter = new ANFListPresenter();
            mPresenter.setCallbackListener(positivePresenter);
            mPresenter.sendResult(promotionList);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    @Test
    public void negativeSendResultTest() {
        try {
            Promotions promotions = new Promotions(SampleData.TEST_DATA);
            promotionList = promotions.getPromotions();

            promotions = new Promotions(SampleData.TEST_DATA_1);
            List<Promotion> promotionList1 = promotions.getPromotions();

            mPresenter = new ANFListPresenter();
            mPresenter.setCallbackListener(negativePresenter);
            mPresenter.sendResult(promotionList1);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    @Test
    public void successfulSendErrorTest() {
        mPresenter = new ANFListPresenter();
        mPresenter.setCallbackListener(positivePresenter);
        mPresenter.sendError("Network unavailable");
    }

    @Test
    public void negativeSendErrorTest() {
        mPresenter = new ANFListPresenter();
        mPresenter.setCallbackListener(negativePresenter);
        mPresenter.sendError("Network available");
    }

    @Test
    public void successfulDetailsTest() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_2);
            promotion = new Promotion(jsonObject);
            mPresenter = new ANFListPresenter();
            mPresenter.setCallbackListener(positivePresenter);
            mPresenter.promotionSelected(promotion);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    @Test
    public void negativeDetailsTest() {
        try {
            JSONObject jsonObject1 = new JSONObject(SampleData.TEST_ITEM_2);
            JSONObject jsonObject2 = new JSONObject(SampleData.TEST_ITEM_1);
            promotion = new Promotion(jsonObject1);
            Promotion promotion2 = new Promotion(jsonObject2);
            mPresenter = new ANFListPresenter();
            mPresenter.setCallbackListener(negativePresenter);
            mPresenter.promotionSelected(promotion2);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    private IPresenterCallback positivePresenter = new IPresenterCallback() {
        @Override
        public void showPromotionDetails(Promotion promotion1) {
            assertNotNull("Promotion shouldn't be null", promotion1);
            assertEquals("Promotion should be same", promotion, promotion1);
        }

        @Override
        public void onSuccess(List<Promotion> promotionList1) {
            assertNotNull("PromotionList shouldn't be null", promotionList1);
            assertEquals("PromotionList should be same", promotionList, promotionList1);
        }

        @Override
        public void onFailure(String errorMessage) {
            assertEquals("Message should be same", errorMsg, errorMessage);
            mPresenter.checkOfflineData("",errorMessage);
        }

        @Override
        public void onProcessingStarted() {
            assertTrue("Should show progress", showProgress);
        }

        @Override
        public void onCacheFailed(String errorMessage) {
            assertEquals("Message should be same", errorMsg, errorMessage);
        }


    };

    private IPresenterCallback negativePresenter = new IPresenterCallback() {

        @Override
        public void showPromotionDetails(Promotion promotion1) {
            assertNotNull("Promotion shouldn't be null", promotion1);
            assertNotEquals("Message should not be same", promotion, promotion1);
        }

        @Override
        public void onSuccess(List<Promotion> promotionList1) {
            assertNotNull("PromotionList shouldn't be null", promotionList1);
            assertNotEquals("PromotionList should not be same", promotionList, promotionList1);
        }

        @Override
        public void onFailure(String errorMessage) {
            assertNotEquals("Message should be same", errorMsg, errorMessage);
            mPresenter.checkOfflineData(null,errorMessage);
        }

        @Override
        public void onProcessingStarted() {
            assertFalse("Shouldn't show progress", showProgress);
        }

        @Override
        public void onCacheFailed(String errorMessage) {
            assertNotEquals("Message should be same", errorMsg, errorMessage);
        }
    };
}
