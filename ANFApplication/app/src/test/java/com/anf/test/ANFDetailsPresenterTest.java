package com.anf.test;

import android.text.Html;
import android.text.Spanned;

import com.anf.test.listeners.IDetailsPresenterCallback;
import com.anf.test.model.Promotion;
import com.anf.test.presenter.ANFDetailsPresenter;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class ANFDetailsPresenterTest {

    private final String expectedTitle = "Dolce Vita";
    private final Spanned expectedFooter = Html.fromHtml("In stores & online. Exclusions apply");
    private final Spanned expectedDescription = Html.fromHtml("Our Favorite Brands");

    private final String expectedFooterString = "In stores & online. Exclusions apply";
    private final String expectedDescString = "Our Favorite Brands";

    private final String expectedImage = "http://anf.scene7.com/is/image/anf/anf-US-20150629-app-women-brands";
    private final String expectedTarget = "https://m.hollisterco.com";
    private String expectedButtonTitle = "Shop Now";

    private final String expectedError = "Invalid text";

    private boolean hideButton = true;

    @Test
    public void successfulFragmentVisibleTest() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_2);
            Promotion promotion = new Promotion(jsonObject);
            promotion.setFooter("In stores & online. Exclusions apply");
            validateUpdates(promotion, mPositiveTestListener);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    @Test
    public void negativeFragmentVisibleTest() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_1);
            Promotion promotion = new Promotion(jsonObject);
            expectedButtonTitle = "Buy now";
            validateUpdates(promotion, mNegativeTestListener);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    @Test
    public void successfulDisplayTargetTest() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_2);
            Promotion promotion = new Promotion(jsonObject);
            validateTargetDisplay(promotion, mPositiveTestListener);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    @Test
    public void negativeDisplayTargetTest() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_ITEM_1);
            Promotion promotion = new Promotion(jsonObject);
            validateTargetDisplay(promotion, mNegativeTestListener);
        } catch (JSONException je) {
            fail("Incorrect json");
        }
    }

    private void validateTargetDisplay(Promotion promotion, IDetailsPresenterCallback callback) {
        ANFDetailsPresenter presenter = new ANFDetailsPresenter(promotion);
        presenter.setCallbackListener(callback);
        presenter.onButtonClicked();
    }

    private void validateUpdates(Promotion promotion, IDetailsPresenterCallback callback) {
        ANFDetailsPresenter presenter = new ANFDetailsPresenter(promotion);
        presenter.setCallbackListener(callback);
        presenter.onFragmentVisible();
    }


    private IDetailsPresenterCallback mPositiveTestListener = new IDetailsPresenterCallback() {
        @Override
        public void displayTarget(String url) {
            assertEquals("Text doesn't match", expectedTarget, url);
        }

        @Override
        public void showToast(String error) {
            assertEquals("Text doesn't match", expectedError, error);
        }

        @Override
        public void updateTitle(String text) {
            assertEquals("Text doesn't match", expectedTitle, text);
        }

        @Override
        public void updateDescription(String text) {
            assertEquals("Text doesn't match", expectedDescString, text);
        }

        @Override
        public void updateFooter(String text) {
            assertEquals("Text doesn't match", expectedFooterString, text);
        }

        @Override
        public void updateDescription(Spanned text) {
            assertEquals("Text doesn't match", expectedDescription, text);
        }

        @Override
        public void updateFooter(Spanned text) {
            assertEquals("Text doesn't match", expectedFooter, text);
        }

        @Override
        public void updateImage(String url) {
            assertEquals("Text doesn't match", expectedImage, url);
        }

        @Override
        public void updateButton(String text) {
            assertEquals("Text doesn't match", expectedButtonTitle, text);
        }

        @Override
        public void hideButton() {
            assertTrue("Text doesn't match", hideButton);
        }
    };

    private IDetailsPresenterCallback mNegativeTestListener = new IDetailsPresenterCallback() {
        @Override
        public void displayTarget(String url) {
            assertNotEquals("Text shouldn't match", expectedTarget, url);
        }

        @Override
        public void showToast(String error) {
            assertNotEquals("Text shouldn't match", expectedError, error);
        }

        @Override
        public void updateTitle(String text) {
            assertNotEquals("Text shouldn't match", expectedTitle, text);
        }

        @Override
        public void updateDescription(String text) {
            assertNotEquals("Text shouldn't match", expectedDescString, text);
        }

        @Override
        public void updateFooter(String text) {
            assertNotEquals("Text shouldn't match", expectedFooterString, text);
        }

        @Override
        public void updateDescription(Spanned text) {
            System.out.print(text);
            assertNotEquals("Text shouldn't match", expectedDescription, text);
        }

        @Override
        public void updateFooter(Spanned text) {
            assertNotEquals("Text shouldn't match", expectedFooter, text);
        }

        @Override
        public void updateImage(String url) {
            assertNotEquals("Text shouldn't match", expectedImage, url);
        }

        @Override
        public void updateButton(String text) {
            assertNotEquals("Text shouldn't match", expectedButtonTitle, text);
        }

        @Override
        public void hideButton() {
            assertFalse("Text doesn't match", hideButton);
        }
    };

}
