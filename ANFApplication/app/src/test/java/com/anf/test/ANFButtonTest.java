package com.anf.test;

import com.anf.test.SampleData;
import com.anf.test.model.ANFButton;
import com.anf.test.model.Promotion;
import com.anf.test.model.Promotions;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class ANFButtonTest {

    ANFButton mButton;

    @Test
    public void emptyPromotionTest1() {
        mButton = new ANFButton();
        asserts();
    }

    @Test
    public void emptyPromotionTest2() {
        try {
            JSONObject jsonObject = new JSONObject("{}");
            mButton = new ANFButton(jsonObject);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }
        assertNotNull("Button should not be null", mButton);
        assertEquals("Titles should be same", "",  mButton.getTitle());
        assertEquals("Target should be same", "", mButton.getTarget());
    }

    @Test
    public void nullPromotionTest() {
        mButton = new ANFButton(null);
        asserts();
    }

    @Test
    public void randomPromotionsTest() {
        boolean flag = false;
        try {
            JSONObject jsonObject = new JSONObject("{sdflkafl_sdjflas{]}");
            mButton = new ANFButton(jsonObject);
        } catch (JSONException je) {
            flag = true;
        }
        assertTrue("Must throw Exception", flag);
        assertNull("Button should be null", mButton);
    }

    @Test
    public void promotionSuccessTest1() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_Button_1);
            mButton = new ANFButton(jsonObject);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }
        assertNotNull("Button should not be null", mButton);
        assertEquals("Titles should be same", "Shop Now",  mButton.getTitle());
        assertEquals("Target should be same", "https://m.abercrombie.com",  mButton.getTarget());
    }

    @Test
    public void promotionSuccessTest2() {
        try {
            JSONObject jsonObject = new JSONObject(SampleData.TEST_Button_2);
            mButton = new ANFButton(jsonObject);
        } catch (JSONException je) {
            fail("Must not throw an Exception");
        }

        assertNotNull("Button should not be null", mButton);
        assertEquals("Titles should be same", "Shop Now",  mButton.getTitle());
        assertEquals("Targets should be same", "https://m.hollisterco.com", mButton.getTarget());
    }

    public void asserts(){
        assertNotNull("Button should not be null", mButton);
        assertNull("Title should be null", mButton.getTitle());
        assertNull("Target should be null", mButton.getTarget());
    }
}
