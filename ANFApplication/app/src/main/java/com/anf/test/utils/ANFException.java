package com.anf.test.utils;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class ANFException extends Exception {

    public ANFException(String message){
        super(message);
    }
}
