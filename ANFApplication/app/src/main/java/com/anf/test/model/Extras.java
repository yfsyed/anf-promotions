package com.anf.test.model;

/**
 * Created by ysyed2 on 6/21/16.
 */
public class Extras {

    public static final String ANF = "com.anf.test";

    public static final String ANF_BASE_URL = "https://www.abercrombie.com";

    public static final String ANF_PROMOTION_ENDPOINT = "/anf/nativeapp/Feeds/promotions.json";

    public static final String ANF_PROMOTION_URL = ANF_BASE_URL + ANF_PROMOTION_ENDPOINT;

    public static final String PROMOTION_DATA = "promotions_data";

    public static final String PROMOTIONS = "promotions_extra";

}
