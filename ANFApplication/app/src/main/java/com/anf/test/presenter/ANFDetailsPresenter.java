package com.anf.test.presenter;

import android.text.Html;
import android.text.TextUtils;

import com.anf.test.listeners.IDetailsPresenterCallback;
import com.anf.test.model.ANFButton;
import com.anf.test.model.Promotion;

import java.util.List;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public class ANFDetailsPresenter {

    private IDetailsPresenterCallback mListener;

    private Promotion mPromotion;

    public ANFDetailsPresenter(Promotion promotion){
        mPromotion = promotion;
    }

    public void setCallbackListener(IDetailsPresenterCallback listener) {
        mListener = listener;
    }

    public void removeCallback() {
        mListener = null;
    }

    public void onFragmentVisible(){
        updateImage();
        updateButton();
        updateTitle();
        updateDescription();
        updateFooter();
    }

    public void updateTitle(){
        if(mListener != null && mPromotion != null){
            mListener.updateTitle(mPromotion.getTitle());
        }
    }

    public void updateDescription(){
        if(mListener != null && mPromotion != null){
            if(!TextUtils.isEmpty(mPromotion.getDescription())){
                if(Html.fromHtml(mPromotion.getDescription()) == null){
                    mListener.updateDescription(mPromotion.getDescription());
                } else {
                    mListener.updateDescription(Html.fromHtml(mPromotion.getDescription()));
                }
            }
        }
    }

    public void updateFooter(){
        if(mListener != null && mPromotion != null){
            if(!TextUtils.isEmpty(mPromotion.getFooter())){
                if(Html.fromHtml(mPromotion.getFooter()) == null){
                    mListener.updateFooter(mPromotion.getFooter());
                } else {
                    mListener.updateFooter(Html.fromHtml(mPromotion.getFooter()));
                }
            }
        }
    }

    public void updateImage(){
        if(mListener != null && mPromotion != null){
            mListener.updateImage(mPromotion.getImage());
        }
    }

    public void updateButton(){
        if(mListener != null && mPromotion != null) {
            List<ANFButton> buttonList = mPromotion.getAnfButtonList();
            ANFButton anfButton;
            if (buttonList != null && !buttonList.isEmpty() && (anfButton = buttonList.get(0)) != null) {
                String text = anfButton.getTitle();
                if (!TextUtils.isEmpty(text)) {
                    mListener.updateButton(text);
                }
            } else {
                sendError();
            }
        }
    }

    public void sendError(){
        mListener.showToast("No target to navigate");
        mListener.hideButton();
    }

    public void onButtonClicked(){
        if(mListener != null && mPromotion != null){
            List<ANFButton> buttonList = mPromotion.getAnfButtonList();
            ANFButton anfButton;
            if(buttonList != null && !buttonList.isEmpty() && (anfButton = buttonList.get(0)) != null) {
                String url = anfButton.getTarget();
                if(!TextUtils.isEmpty(url)) {
                    mListener.displayTarget(url);
                }
            } else {
                mListener.showToast("No target to navigate");
            }
        }
    }


}
