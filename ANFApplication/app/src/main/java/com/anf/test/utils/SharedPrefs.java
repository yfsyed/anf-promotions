package com.anf.test.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.anf.test.model.Extras;

/**
 * Created by Yousuf Syed on 7/3/16.
 */
public class SharedPrefs {

    public static void saveJSON(Context ctx, String key, String value){
        SharedPreferences sp = ctx.getSharedPreferences(Extras.ANF, Context.MODE_PRIVATE);
        sp.edit().putString(key, value).apply();
    }

    public static String getJSON(Context ctx, String key){
        SharedPreferences sp = ctx.getSharedPreferences(Extras.ANF, Context.MODE_PRIVATE);
        return sp.getString(key,"");
    }

}
