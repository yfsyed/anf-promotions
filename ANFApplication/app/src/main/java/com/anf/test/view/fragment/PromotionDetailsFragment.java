package com.anf.test.view.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anf.test.R;
import com.anf.test.databinding.PromotionDetailsBinding;
import com.anf.test.listeners.IDetailsPresenterCallback;
import com.anf.test.model.Promotion;
import com.anf.test.presenter.ANFDetailsPresenter;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PromotionDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PromotionDetailsFragment extends Fragment implements
        IDetailsPresenterCallback {

    public static final String TAG = PromotionDetailsFragment.class.getSimpleName();

    private static final String PROMOTION = "promotion";

//    private TextView mTitle, mDescription, mFooter;
//
//    private Button mButton;
//
//    private ImageView mImage;

    private Promotion mPromotion;

    private Handler mHandler;

    private ANFDetailsPresenter mDetailsPresenter;

    private PromotionDetailsBinding detailsBinding;

    public PromotionDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A instance of fragment PromotionDetailsFragment.
     */
    public static PromotionDetailsFragment newInstance(FragmentManager fm, Promotion promotion) {
        PromotionDetailsFragment fragment = (PromotionDetailsFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new PromotionDetailsFragment();
        }
        Bundle args = new Bundle();
        args.putParcelable(PROMOTION, promotion);
        fragment.setArguments(args);
        return fragment;
    }

//    public void setPromotionDetails(Promotion promotion) {
//        mPromotion = promotion;
//    }

    public void navButtonClicked(View view) {
        if (mDetailsPresenter != null) {
            mDetailsPresenter.onButtonClicked();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPromotion = getArguments().getParcelable(PROMOTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /**
         * GONE ARE THE DAYS WHEN WE USED TO GET ALL THE REFERENCES FOR VIEWS.
         */
        //View root = inflater.inflate(R.layout.promotion_details, container, false);

        detailsBinding = DataBindingUtil.inflate(inflater, R.layout.promotion_details, container, false);
        View root = detailsBinding.getRoot();
        mDetailsPresenter = new ANFDetailsPresenter(mPromotion);
        mHandler = new Handler();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDetailsPresenter != null) {
            mDetailsPresenter.setCallbackListener(this);
            mDetailsPresenter.onFragmentVisible();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDetailsPresenter != null) {
            mDetailsPresenter.removeCallback();
        }
    }

    @Override
    public void displayTarget(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        getContext().startActivity(intent);
    }

    @Override
    public void showToast(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateTitle(final String title) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productTitle.setText(title);
            }
        });
    }

    @Override
    public void updateDescription(final String description) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productDescription.setText(description);
            }
        });
    }

    @Override
    public void updateFooter(final String footer) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productFooter.setText(footer);
                detailsBinding.productFooter.setMovementMethod(LinkMovementMethod.getInstance());
            }
        });
    }

    @Override
    public void updateDescription(final Spanned description) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productDescription.setText(description);
            }
        });
    }

    @Override
    public void updateFooter(final Spanned footer) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productFooter.setText(footer);
                detailsBinding.productFooter.setMovementMethod(LinkMovementMethod.getInstance());
            }
        });
    }

    @Override
    public void updateImage(final String url) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getContext())
                        .load(url)
                        .into(detailsBinding.productImage);
            }
        });
    }

    @Override
    public void updateButton(final String text) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productButton.setText(text);
            }
        });
    }

    @Override
    public void hideButton() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                detailsBinding.productButton.setVisibility(View.GONE);
            }
        });
    }

}
