package com.anf.test.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yousuf Syed on 6/28/16.
 */
public class Promotion implements Parcelable {

    private List<ANFButton> anfButtonList;
    private String description;
    private String footer;
    private String image;
    private String title;

    public Promotion() {
        anfButtonList = new ArrayList<>();
    }

    public Promotion(JSONObject jsonObject) {
        this();
        parseJson(jsonObject);
    }

    private void parseJson(JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }

        description = jsonObject.optString("description");
        title = jsonObject.optString("title");
        footer = jsonObject.optString("footer");
        image = jsonObject.optString("image");

        parseAnfButtons(jsonObject.opt("button"));

    }

    public void parseAnfButtons(Object jsonData) {
        ANFButton anfButton;

        if (jsonData instanceof JSONObject) {
            anfButton = new ANFButton((JSONObject) jsonData);
            anfButtonList.add(anfButton);
        } else if (jsonData instanceof JSONArray) {
            JSONArray buttonsArray = (JSONArray) jsonData;
            for (int i = 0; i < buttonsArray.length(); i++) {
                JSONObject promotionJson = buttonsArray.optJSONObject(i);
                anfButton = new ANFButton(promotionJson);
                anfButtonList.add(anfButton);
            }
        }
    }

    /**
     * Get list of Buttons for the promotion.
     *
     * @return
     */
    public List<ANFButton> getAnfButtonList() {
        return anfButtonList;
    }

    /**
     * Add Button to the list
     *
     * @param anfButton
     */
    public void addAnfButtonToList(ANFButton anfButton) {
        if(anfButtonList != null) {
            anfButtonList.add(anfButton);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.anfButtonList);
        dest.writeString(this.description);
        dest.writeString(this.footer);
        dest.writeString(this.image);
        dest.writeString(this.title);
    }

    protected Promotion(Parcel in) {
        this.anfButtonList = in.createTypedArrayList(ANFButton.CREATOR);
        this.description = in.readString();
        this.footer = in.readString();
        this.image = in.readString();
        this.title = in.readString();
    }

    public static final Parcelable.Creator<Promotion> CREATOR = new Parcelable.Creator<Promotion>() {
        @Override
        public Promotion createFromParcel(Parcel source) {
            return new Promotion(source);
        }

        @Override
        public Promotion[] newArray(int size) {
            return new Promotion[size];
        }
    };
}

