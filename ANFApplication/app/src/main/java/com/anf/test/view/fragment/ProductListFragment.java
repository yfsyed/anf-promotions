package com.anf.test.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anf.test.R;
import com.anf.test.adapter.ProductListAdapter;
import com.anf.test.databinding.ProductListBinding;
import com.anf.test.listeners.IPresenterCallback;
import com.anf.test.listeners.ItemClickListener;
import com.anf.test.listeners.ItemSelectionListener;
import com.anf.test.model.Extras;
import com.anf.test.model.Promotion;
import com.anf.test.presenter.ANFListPresenter;
import com.anf.test.utils.SharedPrefs;

import java.util.List;

/**
 * To Display List of Promotions
 */
public class ProductListFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener,
        IPresenterCallback, ItemClickListener {

    public static final String TAG = ProductListFragment.class.getSimpleName();

    private ANFListPresenter mPresenter;

    private ProductListAdapter mAdapter;

    private Handler mHandler;

    ProductListBinding productListBinding;

    public ProductListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment ProductListFragment.
     */
    public static ProductListFragment newInstance(FragmentManager fm) {
        ProductListFragment fragment = (ProductListFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new ProductListFragment();
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // View root = inflater.inflate(R.layout.product_list, container, false);

        //Using databinding...
        productListBinding = DataBindingUtil.inflate(inflater, R.layout.product_list, container, false);
        View root =  productListBinding.getRoot();

        mAdapter = new ProductListAdapter();
        mAdapter.setItemClickListener(this);
        productListBinding.productsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        productListBinding.productsList.setAdapter(mAdapter);

        productListBinding.refreshPromotions.setOnRefreshListener(this);
        productListBinding.refreshPromotions.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorPrimaryDark, R.color.colorAccent);

        mPresenter = new ANFListPresenter();
        mHandler = new Handler();
        mPresenter.setCallbackListener(this);
        onListRefreshTriggered();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.setCallbackListener(this);
        }
    }

    @Override
    public void onPause() {
        if (mPresenter != null) {
            mPresenter.persistPromotions(getActivity().getApplicationContext());
            mPresenter.removeCallback();
        }
        super.onPause();
    }

    @Override
    public void onRefresh() {
        mPresenter.persistPromotions(getActivity().getApplicationContext());
        onListRefreshTriggered();
    }

    /**
     * Ask presenter what to do when Refresh was triggered by user.
     */
    public void onListRefreshTriggered() {
        if (mPresenter != null) {
            mPresenter.getPromotionList();
        }
    }

    /************************************************************
     * *
     * Callbacks from Presenter                *
     * *
     ************************************************************/

    @Override
    public void onSuccess(final List<Promotion> promotionList) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                productListBinding.refreshPromotions.setRefreshing(false);
                productListBinding.emptyPromotionsView.setVisibility(View.GONE);
                mAdapter.setProductList(promotionList);
            }
        });
    }

    @Override
    public void onFailure(final String error) {
        String persistedJson = SharedPrefs.getJSON(getActivity().getApplicationContext(), Extras.PROMOTIONS);
        mPresenter.checkOfflineData(persistedJson, error);
    }

    @Override
    public void onProcessingStarted() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                productListBinding.emptyPromotionsView.setVisibility(View.GONE);
                productListBinding.refreshPromotions.setRefreshing(true);
            }
        });
    }

    @Override
    public void onCacheFailed(final String error) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                productListBinding.refreshPromotions.setRefreshing(false);
                mAdapter.setProductList(null);
                productListBinding.emptyPromotionsView.setVisibility(View.VISIBLE);
                productListBinding.emptyPromotionsView.setText(error);
            }
        });

    }

    @Override
    public void showPromotionDetails(Promotion promotion) {
        if (getActivity() instanceof ItemSelectionListener) {
            ItemSelectionListener listener = (ItemSelectionListener) getActivity();
            listener.showPromotionDetails(promotion);
        }
    }

    /************************************************************
     * *
     * Callbacks from Adapter                  *
     * *
     ************************************************************/

    @Override
    public void onItemClicked(Promotion promotion) {
        if (mPresenter != null) {
            mPresenter.promotionSelected(promotion);
        }
    }

}
