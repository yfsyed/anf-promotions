package com.anf.test.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by Yousuf Syed on 6/28/16.
 */
public class ANFButton implements Parcelable {

    private String target;

    private String title;

    protected ANFButton(Parcel in) {
        this.target = in.readString();
        this.title = in.readString();
    }

    public ANFButton(){

    }

    public ANFButton(JSONObject jsonObject){
        parseJson(jsonObject);
    }

    private void parseJson(JSONObject jsonObject){
        if(jsonObject == null)
            return;

        target = jsonObject.optString("target");
        title = jsonObject.optString("title");
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.target);
        dest.writeString(this.title);
    }

    public static final Parcelable.Creator<ANFButton> CREATOR = new Parcelable.Creator<ANFButton>() {
        @Override
        public ANFButton createFromParcel(Parcel source) {
            return new ANFButton(source);
        }

        @Override
        public ANFButton[] newArray(int size) {
            return new ANFButton[size];
        }
    };
}
