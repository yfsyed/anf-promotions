package com.anf.test.listeners;

import com.anf.test.model.Promotions;

/**
 * Created by Yousuf Syed on 6/28/16.
 */
public interface IPromotionRequestListener {

    void onSuccess(Promotions promotions);

    void onFailure(String errorMessage);
}
