package com.anf.test.utils;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.anf.test.listeners.IPromotionRequestListener;
import com.anf.test.model.Promotions;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Yousuf Syed on 6/28/16.
 * <p/>
 * To fetch the promotions asynchronously and update listener with results.
 */
public class PromotionRequestTask extends AsyncTask<String, Void, Promotions> {


    private IPromotionRequestListener mListener;  //Listener to communication with the view.

    /**
     * Set listener for commands from presenter.
     *
     * @param listener View object interested in listening to callbacks
     */
    public PromotionRequestTask(IPromotionRequestListener listener) {
        mListener = listener;
    }

    @Override
    protected Promotions doInBackground(String... params) {
        Promotions promotions;
        try {
            String response = getPromotions(params[0]);
            promotions = new Promotions(response);
        } catch (IOException ioe) {
            promotions = getPromotionsWithErrorMessage("Network Unavailable");
        } catch (JSONException ioe) {
            promotions = getPromotionsWithErrorMessage("Data Parsing Failed");
        } catch (NullPointerException npe) {
            promotions = getPromotionsWithErrorMessage("Data Unavailable");
        } catch (ANFException anfe) {
            promotions = getPromotionsWithErrorMessage(anfe.getMessage());
        }
        return promotions;
    }

    @Override
    protected void onPostExecute(Promotions promotions) {
        super.onPostExecute(promotions);
        if (promotions != null) {
            if (TextUtils.isEmpty(promotions.getErrorMessage())) {
                sendSuccessResponse(promotions);
            } else {
                sendFailureResponse(promotions.getErrorMessage());
            }
        }
        mListener = null;
    }

    @Override
    protected void onCancelled(Promotions promotions) {
        super.onCancelled(promotions);
        mListener = null;
    }

    /**
     * Get error specific Promotions object
     *
     * @param errorMessage Error message string
     * @return Promotions object
     */
    private Promotions getPromotionsWithErrorMessage(String errorMessage) {
        Promotions promotions = new Promotions();
        promotions.setErrorMessage(errorMessage);
        return promotions;
    }

    /**
     * send successful response to the view
     *
     * @param response promotions data
     */
    private void sendSuccessResponse(Promotions response) {
        if (mListener != null) {
            mListener.onSuccess(response);
        }
    }

    /**
     * Send failure response to the view
     *
     * @param errorMessage errorMessage
     */
    private void sendFailureResponse(String errorMessage) {
        if (mListener != null) {
            mListener.onFailure(errorMessage);
        }
    }

    /**
     * OkHttp Implementation for network calls.
     *
     * @param url url to fetch promotions.
     * @return Response String
     * @throws IOException
     * @throws ANFException
     */
    public String getPromotions(String url) throws IOException, ANFException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            return response.body().string();
        } else {
            throw new ANFException("UnSuccessful Response");
        }
    }

}
