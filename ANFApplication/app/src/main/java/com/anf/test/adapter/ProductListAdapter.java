package com.anf.test.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anf.test.R;
import com.anf.test.listeners.ItemClickListener;
import com.anf.test.model.Promotion;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yousuf Syed on 6/27/16.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductItem> {

    private List<Promotion> mProductItems;

    private ItemClickListener mListener;

    public ProductListAdapter() {
        mProductItems = new ArrayList<Promotion>();
    }

    /**
     * Set onClick listener to receive on click events
     *
     * @param listener listener for onClick events
     */
    public void setItemClickListener(ItemClickListener listener) {
        mListener = listener;
    }

    /**
     * To update the promotion list.
     *
     * @param list promotion list
     */
    public void setProductList(List<Promotion> list) {
        mProductItems.clear();
        if (list != null) {
            mProductItems.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public ProductItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        return new ProductItem(v, mListener, mProductItems);
    }

    @Override
    public int getItemCount() {
        if (mProductItems != null) {
            return mProductItems.size();
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(ProductItem holder, int position) {
        Promotion promotion = mProductItems.get(position);
        holder.mTitle.setText(promotion.getTitle());
        Picasso.with(holder.mTitle.getContext())
                .load(promotion.getImage())
                .into(holder.mImage);
    }


    public static class ProductItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView mImage;

        public TextView mTitle;

        ItemClickListener mListener;

        List<Promotion> mProductItems;
        public ProductItem(View view, ItemClickListener listener, List<Promotion> promotions ) {
            super(view);
            mListener = listener;
            mProductItems = promotions;
            mImage = (ImageView) view.findViewById(R.id.product_image);
            mTitle = (TextView) view.findViewById(R.id.product_title);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (mListener != null && mProductItems != null) {
                int position = getLayoutPosition();
                mListener.onItemClicked(mProductItems.get(position));
            }
        }
    }

}
