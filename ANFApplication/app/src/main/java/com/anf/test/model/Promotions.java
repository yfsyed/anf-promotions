package com.anf.test.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yousuf Syed on 6/28/16.
 */
public class Promotions implements Parcelable {

    private List<Promotion> mPromotions;

    private String errorMessage; // when network request fails

    private int errorCode;  // for localization

    private String persistantJson;

    public Promotions() {
        mPromotions = new ArrayList<>();
    }

    public Promotions(String jsonString) throws JSONException, NullPointerException {
        this();
        persistantJson = jsonString;
        parseJson(jsonString);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Get Promotions
     *
     * @return
     */
    public List<Promotion> getPromotions() {
        return mPromotions;
    }

    /**
     * Add promotion to the list
     *
     * @param promotion
     */
    public void addPromotions(Promotion promotion) {
        if (promotion != null) {
            mPromotions.add(promotion);
        }
    }

    public String getPersistentJson() {
        return persistantJson;
    }

    public void setPersistentJson(String persistantJson) {
        this.persistantJson = persistantJson;
    }

    /**
     * Parse json data
     *
     * @param jsonString
     */
    private void parseJson(String jsonString) throws JSONException, NullPointerException {
        if (jsonString == null) {
            throw new NullPointerException();
        }

        JSONObject jsonObject = new JSONObject(jsonString);
        Promotion promotionObject;
        JSONArray promotions = jsonObject.optJSONArray("promotions");
        for (int i = 0; i < promotions.length(); i++) {
            JSONObject promotionJson = promotions.optJSONObject(i);
            promotionObject = new Promotion(promotionJson);
            addPromotions(promotionObject);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.mPromotions);
        dest.writeString(this.errorMessage);
        dest.writeInt(this.errorCode);
        dest.writeString(this.persistantJson);
    }

    protected Promotions(Parcel in) {
        this.mPromotions = in.createTypedArrayList(Promotion.CREATOR);
        this.errorMessage = in.readString();
        this.errorCode = in.readInt();
        this.persistantJson = in.readString();
    }

    public static final Parcelable.Creator<Promotions> CREATOR = new Parcelable.Creator<Promotions>() {
        @Override
        public Promotions createFromParcel(Parcel source) {
            return new Promotions(source);
        }

        @Override
        public Promotions[] newArray(int size) {
            return new Promotions[size];
        }
    };
}
