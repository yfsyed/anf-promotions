package com.anf.test.listeners;

import android.content.Context;

import com.anf.test.model.Promotion;

import java.util.List;

/**
 * Created by Yousuf Syed on 6/28/16.
 */
public interface IPresenterCallback {

    void showPromotionDetails(Promotion promotion);

    void onSuccess(List<Promotion> promotionList);

    void onFailure(String errorMessage);

    void onProcessingStarted();

    void onCacheFailed(String error);

}
