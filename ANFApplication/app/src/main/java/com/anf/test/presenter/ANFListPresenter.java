package com.anf.test.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.anf.test.listeners.IPresenterCallback;
import com.anf.test.listeners.IPromotionRequestListener;
import com.anf.test.model.Extras;
import com.anf.test.model.Promotion;
import com.anf.test.model.Promotions;
import com.anf.test.utils.PromotionRequestTask;
import com.anf.test.utils.SharedPrefs;

import org.json.JSONException;

import java.util.List;

/**
 * Created by Yousuf Syed on 6/27/16.
 */
public class ANFListPresenter implements IPromotionRequestListener {

    private IPresenterCallback mListener;

    public void setCallbackListener(IPresenterCallback listener) {
        mListener = listener;
    }

    public void removeCallback() {
        mListener = null;
    }

    private String promotionsJson;

    /**
     * Make a network call using Retrofit to fetch data.
     */
    public void getPromotionList() {
        updateProgress();
        PromotionRequestTask promotionRequestTask = new PromotionRequestTask(this);
        promotionRequestTask.execute(Extras.ANF_PROMOTION_URL);
    }

    /**
     *
     * On Successful Response send it to the receiving view
     *
     * @param list
     */
    public void sendResult(List<Promotion> list) {
        if (mListener != null) {
            mListener.onSuccess(list);
        }
    }

    /**
     * On Error send the error to the View to display proper error message.
     *
     * @param string
     */
    public void sendError(String string) {
        if(mListener != null){
            mListener.onFailure(string);
        }
    }

    /**
     * On Request initiation, send the callback to the view to update itself.
     */
    public void updateProgress() {
        if (mListener != null) {
            mListener.onProcessingStarted();
        }
    }

    public void checkOfflineData(String persistedJson, String error){
        if(mListener != null){
            Promotions promotions = null;
            try {
                promotions = new Promotions(persistedJson);
            }catch (JSONException | NullPointerException npe){

            }
            if(promotions == null){
                mListener.onCacheFailed(error);
            } else {
                mListener.onSuccess(promotions.getPromotions());
            }
        }
    }

    public void promotionSelected(Promotion promotion){
        if (mListener != null) {
            mListener.showPromotionDetails(promotion);
        }
    }

    public void persistPromotions(Context ctx){
        if(!TextUtils.isEmpty(promotionsJson)) {
            SharedPrefs.saveJSON(ctx, Extras.PROMOTIONS, promotionsJson);
        }
    }

    @Override
    public void onSuccess(Promotions promotions) {
        if(promotions != null) {
            promotionsJson = promotions.getPersistentJson();
            sendResult(promotions.getPromotions());
        } else {
            sendError(null);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        sendError(errorMessage);
    }
}
