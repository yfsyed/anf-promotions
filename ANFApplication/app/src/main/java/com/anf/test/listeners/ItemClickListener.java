package com.anf.test.listeners;

import com.anf.test.model.Promotion;

/**
 * Interface to send callbacks to the listeners
 */
public interface ItemClickListener {

    void onItemClicked(Promotion item);
}
