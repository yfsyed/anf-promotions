package com.anf.test.listeners;

import com.anf.test.model.Promotion;

/**
 * Created by Yousuf Syed on 6/29/16.
 */
public interface ItemSelectionListener {

    void showPromotionDetails(Promotion promotion);
}
