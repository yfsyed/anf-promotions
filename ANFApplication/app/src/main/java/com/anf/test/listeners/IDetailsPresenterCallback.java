package com.anf.test.listeners;

import android.text.Spannable;
import android.text.Spanned;

import com.anf.test.model.Promotion;

/**
 * Created by Yousuf Syed on 6/28/16.
 */
public interface IDetailsPresenterCallback {

    void displayTarget(String url);

    void showToast(String error);

    void updateTitle(String text);

    void updateDescription(String text);

    void updateFooter(String text);

    void updateDescription(Spanned text);

    void updateFooter(Spanned text);

    void updateImage(String url);

    void updateButton(String text);

    void hideButton();
}
