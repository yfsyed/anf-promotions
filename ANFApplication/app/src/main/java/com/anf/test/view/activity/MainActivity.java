package com.anf.test.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.anf.test.R;
import com.anf.test.databinding.ActivityMainBinding;
import com.anf.test.listeners.ItemSelectionListener;
import com.anf.test.model.Promotion;
import com.anf.test.view.fragment.ProductListFragment;
import com.anf.test.view.fragment.PromotionDetailsFragment;

public class MainActivity extends AppCompatActivity implements ItemSelectionListener{

    private boolean isSavedInstance = false;

    /**
     * Setup Toolbar to display as Action bar.
     */
    private void setupActionBar(ActivityMainBinding binding) {
        setSupportActionBar(binding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.app_name);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_promotion);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupActionBar(binding);

        if (savedInstanceState != null) {
            isSavedInstance = true;
        }

        displayFeedsList();
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count > 1) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void displayFeedsList() {
        FragmentManager fm = getSupportFragmentManager();
        if (isSavedInstance) {
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment instanceof PromotionDetailsFragment) {
                fm.beginTransaction().remove(fragment).commit();
                fm.popBackStack();
            }
        } else {  // new instance
          //  fm.popBackStack();
            ProductListFragment listFragment = ProductListFragment.newInstance(fm);
            fm.beginTransaction().add(R.id.fragment_container, listFragment, ProductListFragment.TAG).commit();
        }
    }

    @Override
    public void showPromotionDetails(Promotion data) {
        PromotionDetailsFragment detailFragment = PromotionDetailsFragment.newInstance(getSupportFragmentManager(), data);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, detailFragment, PromotionDetailsFragment.TAG);
        ft.addToBackStack(PromotionDetailsFragment.TAG).commit();
    }

}
