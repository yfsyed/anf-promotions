# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary


### How do I get set up? ###

* Summary of set up:

*   To import the application into Android Studio launch Android studio and select **"import project   (gradle, eclipse, etc)"**
 
*   To run the application select the app module configuration and click run button on the top. 

*   You can either run it on the physical device by connecting it to the laptop through a usb cable or run on GenyMotion or Emulator.


* How to run tests
  The unit test are added to src/test and to run them you first have to go to build variant and from the dropdown select **Unit tests**.

 To run all tests, from the project structure right click on **com.anf.test** package under src/test/java/ and click **Run tests in com.anf.test**

 To run tests in a specific class, from the project structure right click on test class under com.anf.test package under src/test/java/ and click **Run <Class name>** 

for eg: if Testing ANFButtonTest, right click on ANFButtonTest and click on Run ANFButtonTest.![Screen Shot 2016-06-30 at 1.32.57 AM.png](https://bitbucket.org/repo/dL4d4o/images/2774468120-Screen%20Shot%202016-06-30%20at%201.32.57%20AM.png)


### Who do I talk to? ###

Yousuf Syed
myname.yousuf@gmail.com
